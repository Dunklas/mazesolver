#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <limits.h>

int queryLine(char *msg, char *buf, int n) {

    printf("%s", msg);

    if ((fgets(buf, n, stdin) == NULL)) {
        return 0;
    } else {
        // Remove trailing newline
        int l = strlen(buf);
        if (buf[l-1] == '\n')
           buf[l-1] = '\0';
        return 1;
    }
}

int measureMatrix(FILE *fp, unsigned int *width, unsigned int *height) {

    unsigned long w = 0L;
    unsigned long h = 0L;

    int c;
    while ((c = fgetc(fp)) != EOF && c != '\n') {
        if (c != ' ' && c != '\t')
            w++;
    }

    while (c != EOF) {
        if (c == '\n')
            h++;

        c = fgetc(fp);
    }

    rewind(fp);

    if (w > UINT_MAX || h > UINT_MAX)
        return 0;
    else {
        *width = w;
        *height = h;

        return 1;
    }
}

void trim(char *str) {
    
    int l = strlen(str);
    char tmp[l+1];

    int i = 0, j = 0;
    while (str[i] != '\0') {
        if (str[i] != ' ' && str[i] != '\t' && str[i] != '\n')
            tmp[j++] = str[i];
        i++;
    }
    tmp[j++] = '\0';
    strncpy(str, tmp, j);
}

_Bool isValidRow(char *str) {
    int i = 0;
    while (str[i] != '\0') {
        if (str[i] != '0' && str[i] != '1')
            return 0;
        i++;
    }

    return 1;
}
