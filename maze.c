#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "maze.h"
#include "util.h"

/*
 * An explanation to the buildMaze method..
 *
 *  - 1. Measure width and height of the input file.
 *  - 2. Check if width or height exceed max size, if so, return error.
 *  - 3. Declare a two dimensional char array with measured width/height.
 *  - 4. Read input file row by row.
 *        - Check for illegal characters, if so, return error.
 *        - Check for width longer or shorter than previously measured width, if so, return error.
 *        - Copy each value from the row into two dimensional char array.
 *  - 5. Check if entrance is blocked, if so, return error.
 *  - 6. Check if exit is blocked, if so, return error.
 *  - 7. Allocate memory for maze struct (and its properties).
 *        - If memory allocation failed, return error.
 *        - Copy values from two dimensional array into newly allocated memory.
 *
 * The reason why I'm reading all input to a two dimensional array first is
 * because I don't want to bother freeing up memory if the error checks
 * don't pass.
 */

status_m buildMaze(FILE *fp, maze **m) {

   unsigned int width = 0;
   unsigned int height = 0;
   
   if (!measureMatrix(fp, &width, &height))
       return MAX_SIZE_EXCEEDED;

   char matrix[height][width];

   char *lbuf = NULL;
   size_t size = 0;

   int lineC = 0;
   while ((getline(&lbuf, &size, fp)) > 0) {
       trim(lbuf);
       
       if (strlen(lbuf) != width) {
           free(lbuf);
           return INCONSISTENT_WIDTH;
       }

       if (!isValidRow(lbuf)) {
           free(lbuf);
           return ILLEGAL_CHARACTER;
       }

       for (int i = 0; lbuf[i] != '\0'; i++) {
           matrix[lineC][i] = lbuf[i];
       }
       lineC++;
       free(lbuf);
       lbuf = NULL;
   }
   free(lbuf);

   if (matrix[0][0] != '0') {
       return ENTRANCE_BLOCKED;
   }

   if (matrix[height-1][width-1] != '0') {
       return EXIT_BLOCKED;
   }

   // Error checks (apart from MEMORY_ALLOCATION_FAILED) are completed
   // Proceed with allocating memory

   if ((*m = (maze *) malloc(sizeof(maze))) == NULL) {
       return MEMORY_ALLOCATION_FAILED;
   }

   if (((*m)->map = (char **) malloc(height * sizeof(char *))) == NULL) {
       return MEMORY_ALLOCATION_FAILED;
   }

   for (int i = 0; i < height; i++) {
       if (((*m)->map[i] = (char *) malloc(width * sizeof(char))) == NULL) {
           return MEMORY_ALLOCATION_FAILED;
       } else {
           for (int j = 0; j < width; j++) {
               (*m)->map[i][j] = matrix[i][j];
           }
       }
   }

   (*m)->width = width;
   (*m)->height = height;
   
   coord tmp_exit = {
       .row = height-1,
       .col = width-1
   };
   (*m)->exit = tmp_exit;
   
   coord tmp_entrance = {
       .row = 0,
       .col = 0
   };
   (*m)->entrance = tmp_entrance;
   
   return MAZE_OK;
}

void clearMaze(maze **m) {
    for (int i = 0; i < (*m)->height; i++) {
        free((*m)->map[i]);
    }
    free((*m)->map);
    free(*m);
}

_Bool cEquals(coord c1, coord c2) {
    return c1.row == c2.row &&
        c1.col == c2.col;
}

void printCoord(coord c) {
    printf("(%d, %d)", c.row, c.col);
}

void printMaze(maze *m) {
    printf("\n");
    for (int i = 0; i < m->height; i++) {
        for (int j = 0; j < m->width; j++) {
            printf("[%c]", m->map[i][j]);
        }
        printf("\n");
    }
}
