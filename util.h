/*
 * Prints msg and reads n characters from stdin
 * to buf. Returns 0 if no characters were entered (EOF),
 * and 1 if input was queried successfully.
 *
 * @param   msg a message to be printed before query for input
 * @param   buf a char pointer where input is stored
 * @param   n   max amount of characters to be read
 */
int queryLine(char *msg, char *buf, int n);

/*
 * Measures the width and height of a matrix
 * stored in a text file. Note that space and tab
 * does not contribute to the width. As a courtesy,
 * fp is rewinded to beginning after measurements has been taken.
 *
 * Returns 1 if measurements were taken successfully
 * Returns 0 if width or height exceeds INT_MAX
 *
 * @param   fp      a pointer to the text file
 * @param   width   a pointer to an int where width will be stored
 * @param   height  a pointer to an int where height will be stored
 */
int measureMatrix(FILE *fp, unsigned int *width, unsigned int *height);

/*
 * Removes all white space characters from
 * a string.
 *
 * @param   str     the string to be trimmed
 */
void trim(char *str);

/*
 * Returns a _Bool indicating if str is a valid
 * row or not. If str contain anything other than
 * '0' or '1', false will be returned.
 *
 * @param   str     the string to be validated
 */
_Bool isValidRow(char *str);
