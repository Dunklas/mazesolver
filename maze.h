/*
 * Represents a coordinate in a maze.
 */
typedef struct {

    int row;
    int col;

} coord;

/*
 * Represents a maze.
 */
typedef struct {

    char **map;

    int width;
    int height;

    coord entrance;
    coord exit;

} maze;

/*
 * Error codes which may occur
 * during maze creation.
 */
typedef enum {
    MAX_SIZE_EXCEEDED, 
    INCONSISTENT_WIDTH,
    ILLEGAL_CHARACTER,
    ENTRANCE_BLOCKED,
    EXIT_BLOCKED,
    MEMORY_ALLOCATION_FAILED,
    MAZE_OK
} status_m;

/*
 * Builds a maze based on input from a text file.
 * Returns a status_m indicating if successful
 * or not. To avoid memory leak, always call
 * clearMaze(maze *m) after invoking this method.
 *
 * @param fp    a file pointer to a text file containing a maze
 * @param m     a pointer to a maze struct pointer, where maze will be stored 
 */
status_m buildMaze(FILE *fp, maze **m);

/*
 * Called to free the memory of a previously
 * created maze.
 *
 * @param m     a pointer to a maze struct pointer, which points at the maze which should be cleared
 */
void clearMaze(maze **m);

/*
 * Returns a _Bool indicating if
 * c1 and c2 are equal.
 *
 * @param c1    first coord
 * @param c2    second coord
 */
_Bool cEquals(coord c1, coord c2);

/*
 * Prints a coordinate on stdout.
 *
 * @param c     coord to be printed
 */
void printCoord(coord c);

/*
 * Prints a maze.
 *
 * @param m     maze to be printed
 */
void printMaze(maze *m);
