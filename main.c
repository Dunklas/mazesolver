#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/time.h>

#include "util.h"
#include "maze.h"

#define F_MAX 256
#define ANIM_DELAY 500000 // 0,5s

void findway(maze *m, coord pos, unsigned int *sCount);
void printFrame(maze *m);

// Used to keep track of if flags are enabled
static _Bool printTime;
static _Bool printSolutions;
static _Bool animate;

int main(int argc, char **argv) {

    // Parse eventual flags
    int opt;
    while ((opt = getopt(argc, argv, "tsa")) != -1) {
        switch (opt) {
            case 't': printTime = 1; break;
            case 's': printSolutions = 1; break;
            case 'a': animate = 1; break;
            default:
                fprintf(stderr, "Usage: %s [-tmc]\n", argv[0]);
                exit(EXIT_FAILURE);
        }
    }

    // Variables to measure elapsed time
    struct timeval stop, start;

    _Bool validInput = 0;
    char fname[F_MAX];
    FILE *fp = NULL;
    maze *m = NULL; 
    unsigned int sCount = 0;
    
    while (!validInput) {
        
        if (!queryLine("Please enter filename: ", fname, F_MAX)) {
            printf("\n");
            exit(0);
        }

        fp = fopen(fname, "r");
        if (fp == NULL) {
            fprintf(stderr, "Could not open file.\n");
        } else {
            status_m res = buildMaze(fp, &m);
            if (res == MAZE_OK) {
                validInput = 1;
            } else {
                switch (res) {
                    case MAX_SIZE_EXCEEDED:
                        fprintf(stderr, "Maze is too large.\n");
                        break;
                    case INCONSISTENT_WIDTH:
                        fprintf(stderr, "Width must be same for each row.\n");
                        break;
                    case ILLEGAL_CHARACTER:
                        fprintf(stderr, "Maze may only contain 1's and 0's.\n");
                        break;
                    case ENTRANCE_BLOCKED:
                        fprintf(stderr, "Entrance (top left corner) must have value 0.\n");
                        break;
                    case EXIT_BLOCKED:
                        fprintf(stderr, "Exit (bottom right corner) must have value 0.\n");
                        break;
                    case MEMORY_ALLOCATION_FAILED:
                        fprintf(stderr, "Could not allocate memory for maze.\n");
                        break;
                    default:
                        fprintf(stderr, "This should not be printed, so if it does, the author of this program made a mistake...\n");
                }
            }
        }
    }

    // Measure time and navigate the maze!
    gettimeofday(&start, NULL);
    findway(m, m->entrance, &sCount);
    gettimeofday(&stop, NULL);

    printf("\nThere were %d solutions.\n", sCount);
    if (printTime) {
        printf("Completed in %lu microseconds.\n", stop.tv_usec - start.tv_usec);
    }

    // Free allocated memory
    clearMaze(&m);
    fclose(fp);

    return (0);
}

void findway(maze *m, coord pos, unsigned int *sCount) {

    if (pos.row < 0 || pos.row > m->height-1 ||
            pos.col < 0 || pos.col > m->width-1)
        return;

    if (m->map[pos.row][pos.col] == '1')
        return;

    if (m->map[pos.row][pos.col] == 'x')
        return;

    if (cEquals(pos, m->exit)) {
        (*sCount)++; 
        m->map[pos.row][pos.col] = 'G';
        
        if (printSolutions) {
            printMaze(m);
        } else if (animate) {
            printFrame(m);
        }
        
        return;
    }

    m->map[pos.row][pos.col] = 'x';
    if (animate) {
        printFrame(m);
    }

    coord eastPos = (coord) { .row = pos.row, .col = pos.col+1 };
    findway(m, eastPos, sCount);

    coord westPos = (coord) { .row = pos.row, .col = pos.col-1 };
    findway(m, westPos, sCount);

    coord southPos = (coord) { .row = pos.row+1, .col = pos.col };
    findway(m, southPos, sCount);

    coord northPos = (coord) { .row = pos.row-1, .col = pos.col };
    findway(m, northPos, sCount);

    m->map[pos.row][pos.col] = '0';
    
    if (animate) {
        printFrame(m);
    }
}

// Helper function to print a "frame". Only used if animations (-a flag) are enabled.
void printFrame(maze *m) {
    printf("%c[2J%c[;H", (char) 27, (char) 27);
    printMaze(m);
    usleep(ANIM_DELAY);
}
